package com.brainmagic.gatescatalog.api.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class QuizupdateData {


    @SerializedName("cs1")
    @Expose
    private QuizUpdateCsone cs1;
    @SerializedName("upcomingquizdate")
    @Expose
    private String upcomingquizdate;

    public QuizUpdateCsone getCs1() {
        return cs1;
    }

    public void setCs1(QuizUpdateCsone cs1) {
        this.cs1 = cs1;
    }

    public String getUpcomingquizdate() {
        return upcomingquizdate;
    }

    public void setUpcomingquizdate(String upcomingquizdate) {
        this.upcomingquizdate = upcomingquizdate;
    }

}
