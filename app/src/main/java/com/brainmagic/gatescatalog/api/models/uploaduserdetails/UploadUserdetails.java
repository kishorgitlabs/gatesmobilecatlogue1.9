
package com.brainmagic.gatescatalog.api.models.uploaduserdetails;


import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class UploadUserdetails {

    @SerializedName("data")
    private UploaduserData mData;
    @SerializedName("result")
    private String mResult;

    public UploaduserData getData() {
        return mData;
    }

    public void setData(UploaduserData data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
