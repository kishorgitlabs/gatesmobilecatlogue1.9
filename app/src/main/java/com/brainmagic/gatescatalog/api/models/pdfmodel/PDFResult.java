
package com.brainmagic.gatescatalog.api.models.pdfmodel;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class PDFResult {

    @SerializedName("Id")
    private Long mId;
    @SerializedName("InsertDate")
    private String mInsertDate;
    @SerializedName("PdfName")
    private String mPdfName;
    @SerializedName("PdfURL")
    private String mPdfURL;

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getInsertDate() {
        return mInsertDate;
    }

    public void setInsertDate(String insertDate) {
        mInsertDate = insertDate;
    }

    public String getPdfName() {
        return mPdfName;
    }

    public void setPdfName(String pdfName) {
        mPdfName = pdfName;
    }

    public String getPdfURL() {
        return mPdfURL;
    }

    public void setPdfURL(String pdfURL) {
        mPdfURL = pdfURL;
    }

}
