
package com.brainmagic.gatescatalog.api.models.schemespromo;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class SchemesPromoModel {

    @SerializedName("data")
    private List<SchemesPromoResult> mData;
    @SerializedName("result")
    private String mResult;

    public List<SchemesPromoResult> getData() {
        return mData;
    }

    public void setData(List<SchemesPromoResult> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
