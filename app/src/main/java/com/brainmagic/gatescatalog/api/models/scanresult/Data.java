
package com.brainmagic.gatescatalog.api.models.scanresult;


import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class Data {

    @SerializedName("Address")
    private String mAddress;
    @SerializedName("Datetime")
    private Object mDatetime;
    @SerializedName("id")
    private Long mId;
    @SerializedName("Latitude")
    private String mLatitude;
    @SerializedName("Longitude")
    private String mLongitude;
    @SerializedName("MobileNo")
    private String mMobileNo;
    @SerializedName("PartNo")
    private String mPartNo;
    @SerializedName("SerialNo")
    private String mSerialNo;

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String address) {
        mAddress = address;
    }

    public Object getDatetime() {
        return mDatetime;
    }

    public void setDatetime(Object datetime) {
        mDatetime = datetime;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getLatitude() {
        return mLatitude;
    }

    public void setLatitude(String latitude) {
        mLatitude = latitude;
    }

    public String getLongitude() {
        return mLongitude;
    }

    public void setLongitude(String longitude) {
        mLongitude = longitude;
    }

    public String getMobileNo() {
        return mMobileNo;
    }

    public void setMobileNo(String mobileNo) {
        mMobileNo = mobileNo;
    }

    public String getPartNo() {
        return mPartNo;
    }

    public void setPartNo(String partNo) {
        mPartNo = partNo;
    }

    public String getSerialNo() {
        return mSerialNo;
    }

    public void setSerialNo(String serialNo) {
        mSerialNo = serialNo;
    }

}
