package com.brainmagic.gatescatalog.api.models.PriceList;

import com.google.gson.annotations.SerializedName;

public class OePartList {

	@SerializedName("CatalogPartnumber")
	private String catalogPartnumber;

	@SerializedName("Partno")
	private String partno;

	@SerializedName("Insertdate")
	private String insertdate;

	@SerializedName("MRP")
	private String mRP;

	@SerializedName("Id")
	private int id;

	public void setCatalogPartnumber(String catalogPartnumber){
		this.catalogPartnumber = catalogPartnumber;
	}

	public String getCatalogPartnumber(){
		return catalogPartnumber;
	}

	public void setPartno(String partno){
		this.partno = partno;
	}

	public String getPartno(){
		return partno;
	}

	public void setInsertdate(String insertdate){
		this.insertdate = insertdate;
	}

	public String getInsertdate(){
		return insertdate;
	}

	public void setMRP(String mRP){
		this.mRP = mRP;
	}

	public String getMRP(){
		return mRP;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}
}