package com.brainmagic.gatescatalog.api.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetPointHistory {

    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("Mobile")
    @Expose
    private String mobile;
    @SerializedName("Joiningfacebook")
    @Expose
    private String joiningfacebook;
    @SerializedName("weeklyquiz")
    @Expose
    private String weeklyquiz;
    @SerializedName("Trainingattendance")
    @Expose
    private String trainingattendance;
    @SerializedName("trainingassesment")
    @Expose
    private String trainingassesment;
    @SerializedName("Monthlyperformer")
    @Expose
    private String monthlyperformer;
    @SerializedName("Carpasting")
    @Expose
    private String carpasting;
    @SerializedName("Flangeinstall")
    @Expose
    private String flangeinstall;
    @SerializedName("Welcomebonus")
    @Expose
    private String welcomebonus;
    @SerializedName("totpoints")
    @Expose
    private String totpoints;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getJoiningfacebook() {
        return joiningfacebook;
    }

    public void setJoiningfacebook(String joiningfacebook) {
        this.joiningfacebook = joiningfacebook;
    }

    public String getWeeklyquiz() {
        return weeklyquiz;
    }

    public void setWeeklyquiz(String weeklyquiz) {
        this.weeklyquiz = weeklyquiz;
    }

    public String getTrainingattendance() {
        return trainingattendance;
    }

    public void setTrainingattendance(String trainingattendance) {
        this.trainingattendance = trainingattendance;
    }

    public String getTrainingassesment() {
        return trainingassesment;
    }

    public void setTrainingassesment(String trainingassesment) {
        this.trainingassesment = trainingassesment;
    }

    public String getMonthlyperformer() {
        return monthlyperformer;
    }

    public void setMonthlyperformer(String monthlyperformer) {
        this.monthlyperformer = monthlyperformer;
    }

    public String getCarpasting() {
        return carpasting;
    }

    public void setCarpasting(String carpasting) {
        this.carpasting = carpasting;
    }

    public String getFlangeinstall() {
        return flangeinstall;
    }

    public void setFlangeinstall(String flangeinstall) {
        this.flangeinstall = flangeinstall;
    }

    public String getWelcomebonus() {
        return welcomebonus;
    }

    public void setWelcomebonus(String welcomebonus) {
        this.welcomebonus = welcomebonus;
    }

    public String getTotpoints() {
        return totpoints;
    }

    public void setTotpoints(String totpoints) {
        this.totpoints = totpoints;
    }
}
