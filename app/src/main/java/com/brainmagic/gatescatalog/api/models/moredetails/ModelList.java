
package com.brainmagic.gatescatalog.api.models.moredetails;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class ModelList {

    @SerializedName("model")
    private String mModel;
    @SerializedName("produSpec")
    private List<EngineCodeResult> mEngineCodeResult;

    public String getModel() {
        return mModel;
    }

    public void setModel(String model) {
        mModel = model;
    }

    public List<EngineCodeResult> getProduSpec() {
        return mEngineCodeResult;
    }

    public void setProduSpec(List<EngineCodeResult> mEngineCodeResult) {
        this.mEngineCodeResult = mEngineCodeResult;
    }

}
