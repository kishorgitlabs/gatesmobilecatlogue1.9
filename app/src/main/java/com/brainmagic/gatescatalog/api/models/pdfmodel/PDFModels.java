
package com.brainmagic.gatescatalog.api.models.pdfmodel;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class PDFModels {

    @SerializedName("data")
    private List<PDFResult> mData;
    @SerializedName("result")
    private String mResult;

    public List<PDFResult> getData() {
        return mData;
    }

    public void setData(List<PDFResult> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
