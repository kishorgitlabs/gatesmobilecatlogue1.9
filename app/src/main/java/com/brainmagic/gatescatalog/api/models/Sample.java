package com.brainmagic.gatescatalog.api.models;

public class Sample {

    private String oem;
    private String model;
    private String productType;
    private String segment;
    private String engineSpecification;
    private String partNo;

    public String getOem() {
        return oem;
    }

    public void setOem(String oem) {
        this.oem = oem;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getSegment() {
        return segment;
    }

    public void setSegment(String segment) {
        this.segment = segment;
    }

    public String getEngineSpecification() {
        return engineSpecification;
    }

    public void setEngineSpecification(String engineSpecification) {
        this.engineSpecification = engineSpecification;
    }

    public String getPartNo() {
        return partNo;
    }

    public void setPartNo(String partNo) {
        this.partNo = partNo;
    }

    @Override
    public String toString() {
        return "Sample{" +
                "oem='" + oem + '\'' +
                ", model='" + model + '\'' +
                ", productType='" + productType + '\'' +
                ", segment='" + segment + '\'' +
                ", engineSpecification='" + engineSpecification + '\'' +
                ", partNo='" + partNo + '\'' +
                '}';
    }
}
