package com.brainmagic.gatescatalog.api.models.MechanicAdd;

public class Data{
	private Object approveStatus;
	private String address;
	private Object latitude;
	private String mobileno;
	private String gatesEmpName;
	private Object gstin;
	private Object registrationType;
	private String zipcode;
	private int id;
	private String email;
	private Object password;
	private String status;
	private Object businessName;
	private String insertdate;
	private String usertype;
	private String city;
	private Object longitude;
	private Object date;
	private Object shopname;
	private int welcomeBonus;
	private String state;
	private Object appid;
	private String name;
	private Object country;
	private Object oTPStatus;
	private Object updatetime;
	private Object username;
	private Object location;

	public void setApproveStatus(Object approveStatus){
		this.approveStatus = approveStatus;
	}

	public Object getApproveStatus(){
		return approveStatus;
	}

	public void setAddress(String address){
		this.address = address;
	}

	public String getAddress(){
		return address;
	}

	public void setLatitude(Object latitude){
		this.latitude = latitude;
	}

	public Object getLatitude(){
		return latitude;
	}

	public void setMobileno(String mobileno){
		this.mobileno = mobileno;
	}

	public String getMobileno(){
		return mobileno;
	}

	public void setGatesEmpName(String gatesEmpName){
		this.gatesEmpName = gatesEmpName;
	}

	public String getGatesEmpName(){
		return gatesEmpName;
	}

	public void setGstin(Object gstin){
		this.gstin = gstin;
	}

	public Object getGstin(){
		return gstin;
	}

	public void setRegistrationType(Object registrationType){
		this.registrationType = registrationType;
	}

	public Object getRegistrationType(){
		return registrationType;
	}

	public void setZipcode(String zipcode){
		this.zipcode = zipcode;
	}

	public String getZipcode(){
		return zipcode;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	public void setPassword(Object password){
		this.password = password;
	}

	public Object getPassword(){
		return password;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	public void setBusinessName(Object businessName){
		this.businessName = businessName;
	}

	public Object getBusinessName(){
		return businessName;
	}

	public void setInsertdate(String insertdate){
		this.insertdate = insertdate;
	}

	public String getInsertdate(){
		return insertdate;
	}

	public void setUsertype(String usertype){
		this.usertype = usertype;
	}

	public String getUsertype(){
		return usertype;
	}

	public void setCity(String city){
		this.city = city;
	}

	public String getCity(){
		return city;
	}

	public void setLongitude(Object longitude){
		this.longitude = longitude;
	}

	public Object getLongitude(){
		return longitude;
	}

	public void setDate(Object date){
		this.date = date;
	}

	public Object getDate(){
		return date;
	}

	public void setShopname(Object shopname){
		this.shopname = shopname;
	}

	public Object getShopname(){
		return shopname;
	}

	public void setWelcomeBonus(int welcomeBonus){
		this.welcomeBonus = welcomeBonus;
	}

	public int getWelcomeBonus(){
		return welcomeBonus;
	}

	public void setState(String state){
		this.state = state;
	}

	public String getState(){
		return state;
	}

	public void setAppid(Object appid){
		this.appid = appid;
	}

	public Object getAppid(){
		return appid;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setCountry(Object country){
		this.country = country;
	}

	public Object getCountry(){
		return country;
	}

	public void setOTPStatus(Object oTPStatus){
		this.oTPStatus = oTPStatus;
	}

	public Object getOTPStatus(){
		return oTPStatus;
	}

	public void setUpdatetime(Object updatetime){
		this.updatetime = updatetime;
	}

	public Object getUpdatetime(){
		return updatetime;
	}

	public void setUsername(Object username){
		this.username = username;
	}

	public Object getUsername(){
		return username;
	}

	public void setLocation(Object location){
		this.location = location;
	}

	public Object getLocation(){
		return location;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"approveStatus = '" + approveStatus + '\'' + 
			",address = '" + address + '\'' + 
			",latitude = '" + latitude + '\'' + 
			",mobileno = '" + mobileno + '\'' + 
			",gates_emp_name = '" + gatesEmpName + '\'' + 
			",gstin = '" + gstin + '\'' + 
			",registrationType = '" + registrationType + '\'' + 
			",zipcode = '" + zipcode + '\'' + 
			",id = '" + id + '\'' + 
			",email = '" + email + '\'' + 
			",password = '" + password + '\'' + 
			",status = '" + status + '\'' + 
			",businessName = '" + businessName + '\'' + 
			",insertdate = '" + insertdate + '\'' + 
			",usertype = '" + usertype + '\'' + 
			",city = '" + city + '\'' + 
			",longitude = '" + longitude + '\'' + 
			",date = '" + date + '\'' + 
			",shopname = '" + shopname + '\'' + 
			",welcome_bonus = '" + welcomeBonus + '\'' + 
			",state = '" + state + '\'' + 
			",appid = '" + appid + '\'' + 
			",name = '" + name + '\'' + 
			",country = '" + country + '\'' + 
			",oTPStatus = '" + oTPStatus + '\'' + 
			",updatetime = '" + updatetime + '\'' + 
			",username = '" + username + '\'' + 
			",location = '" + location + '\'' + 
			"}";
		}
}
