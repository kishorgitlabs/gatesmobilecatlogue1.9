package com.brainmagic.gatescatalog.api.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class QuizUpdateCsone {

    @SerializedName("data")
    @Expose
    private Object data;
    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("MobileNo")
    @Expose
    private String mobileNo;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("usertype")
    @Expose
    private Object usertype;
    @SerializedName("earnedpoint")
    @Expose
    private Integer earnedpoint;
    @SerializedName("Source")
    @Expose
    private Object source;
    @SerializedName("quizno")
    @Expose
    private Integer quizno;
    @SerializedName("Flag")
    @Expose
    private Integer flag;
    @SerializedName("Insertdate")
    @Expose
    private Object insertdate;

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Object getUsertype() {
        return usertype;
    }

    public void setUsertype(Object usertype) {
        this.usertype = usertype;
    }

    public Integer getEarnedpoint() {
        return earnedpoint;
    }

    public void setEarnedpoint(Integer earnedpoint) {
        this.earnedpoint = earnedpoint;
    }

    public Object getSource() {
        return source;
    }

    public void setSource(Object source) {
        this.source = source;
    }

    public Integer getQuizno() {
        return quizno;
    }

    public void setQuizno(Integer quizno) {
        this.quizno = quizno;
    }

    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }

    public Object getInsertdate() {
        return insertdate;
    }

    public void setInsertdate(Object insertdate) {
        this.insertdate = insertdate;
    }
}
