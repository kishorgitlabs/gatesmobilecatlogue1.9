
package com.brainmagic.gatescatalog.api.models.moredetails;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;


@SuppressWarnings("unused")
public class YearList implements Serializable {

    @SerializedName("YearFrom")
    private List<String> mYearFrom;
    @SerializedName("YearTill")
    private List<String> mYearTill;

    public List<String> getYearFrom() {
        return mYearFrom;
    }

    public void setYearFrom(List<String> yearFrom) {
        mYearFrom = yearFrom;
    }

    public List<String> getYearTill() {
        return mYearTill;
    }

    public void setYearTill(List<String> yearTill) {
        mYearTill = yearTill;
    }

}
