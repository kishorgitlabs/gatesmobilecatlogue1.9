package com.brainmagic.gatescatalog.api.models.threeexpandablelistview;

import java.util.List;

public class ParentMakeListModel {
    String make;
    List<ChildModelListModel> childList;

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public List<ChildModelListModel> getChildList() {
        return childList;
    }

    public void setChildList(List<ChildModelListModel> childList) {
        this.childList = childList;
    }
}
