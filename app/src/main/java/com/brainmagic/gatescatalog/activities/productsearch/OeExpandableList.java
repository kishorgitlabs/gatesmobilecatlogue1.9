package com.brainmagic.gatescatalog.activities.productsearch;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.transition.ChangeBounds;
import androidx.transition.TransitionManager;

import com.brainmagic.gatescatalog.activities.Price_Activity;
import com.bumptech.glide.Glide;
import com.brainmagic.gatescatalog.gates.R;
import com.brainmagic.gatescatalog.activities.About_Gate_Activity;
import com.brainmagic.gatescatalog.activities.OtherLinksActivity;
import com.brainmagic.gatescatalog.activities.Contact_Details_Activity;
import com.brainmagic.gatescatalog.activities.Distributer_Activity;
import com.brainmagic.gatescatalog.activities.HomeScreenActivity;
import com.brainmagic.gatescatalog.activities.Notification_Activity;
import com.brainmagic.gatescatalog.activities.Schemes_Offers_Page;
import com.brainmagic.gatescatalog.activities.productsearch.whatsnew.WhatsNew_ApplicationActivity;
import com.brainmagic.gatescatalog.adapter.ExpandableAdapter;
import com.brainmagic.gatescatalog.alertbox.Alertbox;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.io.File;


public class OeExpandableList extends AppCompatActivity {
    private TextView ProfileName,navicationtxt;

    private int lastExpandedPosition = -1;
    private ViewGroup includeviewLayout;
    private ImageView profilePicture;
//    private ConstraintLayout signout_relativelayout;
    private LinearLayout signout_relativelayout;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    String Email, Mobno, Usertype,Country;
    private FloatingActionButton back, home;
    private ImageView menu;
    private Alertbox box = new Alertbox(OeExpandableList.this);
    private String gatesPartNumber;
//    private List<Group> groupList,groupListOne;
//    private List<Group> groupListOne;
//    private List<Child> childListOne;
//    private SQLiteDatabase database;

    private ExpandableListView expandablelistview;

    private static final String TAG = "OeExpandableList";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_oe_expandable_list);


        navicationtxt = (TextView) findViewById(R.id.navicationtxt);
        includeviewLayout = findViewById(R.id.profile_logo);
        ProfileName = (TextView) includeviewLayout.findViewById(R.id.profilename);
        profilePicture = (ImageView) includeviewLayout.findViewById(R.id.profilepicture);
        signout_relativelayout =  findViewById(R.id.signout);
        final RelativeLayout userData=includeviewLayout.findViewById(R.id.user_data);
        signout_relativelayout.setVisibility(View.GONE);
        menu = (ImageView) findViewById(R.id.menu);
        back = (FloatingActionButton) findViewById(R.id.back);
        home = (FloatingActionButton) findViewById(R.id.home);
        expandablelistview=findViewById(R.id.expandablelistview);


        myshare = getSharedPreferences("Registration", MODE_PRIVATE);
        editor = myshare.edit();

        Email = myshare.getString("email", "");
        Mobno = myshare.getString("MobNo", "");
        Usertype = myshare.getString("Usertype", "");
        Country = myshare.getString("Country", "");
        ProfileName.setText(myshare.getString("name", "")+" , "+Country);
        final ImageView downArrow=includeviewLayout.findViewById(R.id.down_arrow);
        String navi=getIntent().getStringExtra("navigation");
        String oeNumber=getIntent().getStringExtra("OEnumber");
        gatesPartNumber=getIntent().getStringExtra("GatesPartNo");


        navicationtxt.setText(navi);

        if (!myshare.getString("profile_path", "").equals(""))
            Glide.with(OeExpandableList.this)
                    .load(new File(myshare.getString("profile_path", "")))
                    .into(profilePicture);

        if(myshare.getBoolean("islogin",false))
            signout_relativelayout.setVisibility(View.VISIBLE);
        else
            signout_relativelayout.setVisibility(View.GONE);

        signout_relativelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putBoolean("islogin",false).commit();
                final Snackbar mySnackbar = Snackbar.make(findViewById(R.id.oe_grid_activity), getString(R.string.channel_network_sign_out), Snackbar.LENGTH_INDEFINITE);
                mySnackbar.setActionTextColor(getResources().getColor(R.color.red));
                mySnackbar.setAction(getString(R.string.okay_button), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mySnackbar.dismiss();
                        Intent intent = getIntent();
                        finish();
                        startActivity(intent);
                    }
                });
                mySnackbar.show();


            }
        });

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home = new Intent(OeExpandableList.this, HomeScreenActivity.class);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(home);
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        downArrow.setOnClickListener(new View.OnClickListener() {
            boolean visible;
            @Override
            public void onClick(View v) {
                ChangeBounds changeBounds=new ChangeBounds();
                changeBounds.setDuration(600L);
                TransitionManager.beginDelayedTransition(includeviewLayout,changeBounds);
                visible = !visible;
                if(visible)
                    downArrow.animate().rotation(180).setInterpolator(new LinearInterpolator()).setDuration(500);
                else
                    downArrow.animate().rotation(0).setInterpolator(new LinearInterpolator()).setDuration(500);
                userData.setVisibility(visible? View.VISIBLE: View.GONE);
            }
        });

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int width = metrics.widthPixels;
        expandablelistview.setIndicatorBounds(width - GetPixelFromDips(50), width - GetPixelFromDips(10));

        expandablelistview.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {

//                new getmodels().execute();

                if (lastExpandedPosition != -1
                        && groupPosition != lastExpandedPosition) {
                    expandablelistview.collapseGroup(lastExpandedPosition);
                }
                lastExpandedPosition = groupPosition;
            }
        });

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(OeExpandableList.this, view);

                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });


                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub

                        switch (item.getItemId()) {
                            case R.id.homepop:
                                Intent home = new Intent(OeExpandableList.this, HomeScreenActivity.class);
                                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(home);
                                return true;

                            case R.id.aboutpop:
                                startActivity(new Intent(OeExpandableList.this, About_Gate_Activity.class));
                                return true;
                            case R.id.ptoductpop:
                                startActivity(new Intent(OeExpandableList.this, Product_Activity.class));
                                return true;
                            case R.id.other_link_prod:
                                Intent a = new Intent(OeExpandableList.this, OtherLinksActivity.class);
                                startActivity(a);
                                return true;
//                            case R.id.vediopop:
//                                if (!Usertype.equals("")) {
//                                    Intent f = new Intent(OeExpandableList.this, Video_Category_Activity.class);
//                                    startActivity(f);
//                                }
//                                return true;
                            case R.id.notifipop:
                                startActivity(new Intent(OeExpandableList.this, Notification_Activity.class));
                                return true;
                            case R.id.promo_scheem_pop:
                                startActivity(new Intent(OeExpandableList.this, Schemes_Offers_Page.class));
                                return true;
                            case R.id.pricelist:
                                startActivity(new Intent(OeExpandableList.this, Price_Activity.class));
                                return true;
                            case R.id.whatpop:
                                startActivity(new Intent(OeExpandableList.this, WhatsNew_ApplicationActivity.class));
                                return true;
                            case R.id.disnetpop:
                                startActivity(new Intent(OeExpandableList.this, Distributer_Activity.class));
                                return true;
//                                if (Usertype.equals("Gates Employee")) {
//                                    Intent g = new Intent(OeExpandableList.this, LoginActivity.class).putExtra("from", "dist");
//                                    startActivity(g);
//                                } else {
//                                    box.showAlert(getString(R.string.not_authorized));
//                                }
//                                return true;
                            case R.id.contactpop:
                                startActivity(new Intent(OeExpandableList.this, Contact_Details_Activity.class));
                                return true;
//                            case R.id.editprofilepop:
//                                startActivity(new Intent(Product_Model_List_Activity.this, EditProfileActivity.class));
//                                return true;

                        }

                        return false;
                    }
                });
                popupMenu.inflate(R.menu.popmenu);
                popupMenu.show();
            }
        });

        getProductAndOE();


    }

    private void getProductAndOE()
    {
//        SQLiteHelper sqLiteHelper = new SQLiteHelper(OeExpandableList.this);
//        database = sqLiteHelper.getReadableDatabase();
//        AppDatabase db = getAppDatabase(OeExpandableList.this);
//
////        groupList=new ArrayList<>();
//        groupListOne=new ArrayList<>();
//
//        List<String> makeList = db.ProductsDAO().getMakeList(gatesPartNumber);
//
//        if(makeList.size()!=0) {
//
//            for (String make : makeList) {
////            Group group=new Group();
////            childList=new ArrayList<>();
////            group.setMakeName(make);
//                Group groupOne = new Group();
//                childListOne = new ArrayList<>();
//
//                groupOne.setMakeName(make);
//
////            List<String> model=db.ProductsDAO().getModelList(gatesPartNumber,make);
////            List<String> modelCode=db.ProductsDAO().getModelCodeList(gatesPartNumber,make);
//
//                List<GetOEModel> model = db.ProductsDAO().getModelCodeList(gatesPartNumber, make);
////            String query="Select distinct ModelCode, Model, New from product where Gates_Part_Number = '"+gatesPartNumber+"' and Make = '"+make+"'";
////            Cursor c = database.rawQuery(query, null);
////
////            if(c.moveToFirst())
////            {
////                do{
////                    Child child=new Child();
////                    child.setModel(c.getString(c.getColumnIndex("Model")));
////                    child.setModelCode(c.getString(c.getColumnIndex("Modelcode")));
////                    childList.add(child);
////                }while (c.moveToNext());
////
////                group.setChildList(childList);
////                groupList.add(group);
////            }
//
//                for (GetOEModel model1 : model) {
//                    Child child = new Child();
//                    child.setModel(model1.getModel());
//                    child.setEngineCode(model1.getEngineCode());
//                    child.setNewStatus(model1.getNew());
//                    child.setSegment(model1.getSegment());
//                    childListOne.add(child);
//                }
//                groupOne.setChildList(childListOne);
//                groupListOne.add(groupOne);
//
//            }
//
            inflateExpandableList();
//        }else {
//            final AlertDialog alertDialog = new AlertDialog.Builder(OeExpandableList.this).create();
//            LayoutInflater inflater = getLayoutInflater();
//            View dialogView = inflater.inflate(R.layout.alertbox, null);
//            alertDialog.setView(dialogView);
////        ImageView sucess = (ImageView) dialogView.findViewById(R.id.success);
//            TextView log = (TextView) dialogView.findViewById(R.id.textView1);
//            Button okay = (Button) dialogView.findViewById(R.id.okay);
//            log.setText(getString(R.string.no_supported_products)+gatesPartNumber);
//            okay.setText(getString(R.string.okay));
//            okay.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    alertDialog.dismiss();
//                    finish();
//                }
//            });
//
//            alertDialog.setView(dialogView);
//            alertDialog.show();
//        }

    }

    private void inflateExpandableList()
    {
        expandablelistview.setAdapter(new ExpandableAdapter(OeExpandableList.this));
    }

    public int GetPixelFromDips(float pixels) {
        // Get the screen's density scale
        final float scale = getResources().getDisplayMetrics().density;
        // Convert the dps to pixels, based on density scale
        return (int) (pixels * scale + 0.5f);
    }

}
