package com.brainmagic.gatescatalog.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;


import com.brainmagic.gatescatalog.gates.R;
import com.brainmagic.gatescatalog.activities.productsearch.Product_Activity;
import com.brainmagic.gatescatalog.activities.productsearch.whatsnew.WhatsNew_ApplicationActivity;
import com.brainmagic.gatescatalog.activities.scan.ScanActivity;
import com.brainmagic.gatescatalog.adapter.Notification_Adapter;
import com.brainmagic.gatescatalog.alertbox.Alertbox;
import com.brainmagic.gatescatalog.api.APIService;
import com.brainmagic.gatescatalog.api.RetrofitClient;
import com.brainmagic.gatescatalog.api.models.notification.NotificationModel;
import com.brainmagic.gatescatalog.networkconnection.Network_Connection_Activity;
import com.brainmagic.gatescatalog.progressdialog.CustomProgressDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Notification_Activity extends Activity {

    ImageView menu;
    Notification_Adapter adapter;
    FloatingActionButton home, back;
    ListView n_listview;
    Connection connection;
    Statement stmt;
    ProgressDialog progressDialog;
    ResultSet resultSet;
    ArrayList<String> n_name, n_descb, n_exp_date;
    String Email, Mobno, Usertype;
    SharedPreferences myshare;
    SharedPreferences.Editor editor;
    private Alertbox alertbox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_);


        back = findViewById(R.id.back);
        home = findViewById(R.id.home);
        menu = (ImageView) findViewById(R.id.menu);
        alertbox = new Alertbox(Notification_Activity.this);

        n_listview = (ListView) findViewById(R.id.n_listview);


        myshare = getSharedPreferences("Registration", MODE_PRIVATE);
        editor = myshare.edit();

        Email = myshare.getString("Email", "");
        Mobno = myshare.getString("MobNo", "");
        Usertype = myshare.getString("Usertype", "");


        n_name = new ArrayList<String>();
        n_descb = new ArrayList<String>();
        n_exp_date = new ArrayList<String>();


        checkinternet();


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Notification_Activity.this, HomeScreenActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK)
                );
            }
        });


        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(Notification_Activity.this, view);

                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });


                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub

                        switch (item.getItemId()) {
                            case R.id.homepop:
                                Intent home = new Intent(Notification_Activity.this, HomeScreenActivity.class);
                                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(home);
                                return true;

                            case R.id.aboutpop:
                                startActivity(new Intent(Notification_Activity.this, About_Gate_Activity.class));
                                return true;
                            case R.id.ptoductpop:
                                startActivity(new Intent(Notification_Activity.this, Product_Activity.class));
                                return true;
                            case R.id.other_link_prod:
                                Intent a = new Intent(Notification_Activity.this, OtherLinksActivity.class);
                                startActivity(a);
                                return true;
//                            case R.id.vediopop:
//                                if (!Usertype.equals("")) {
//                                    Intent f = new Intent(Notification_Activity.this, Video_Category_Activity.class);
//                                    startActivity(f);
//                                }
//                                return true;
                            case R.id.scan:
                                startActivity(new Intent(Notification_Activity.this, ScanActivity.class));
                                return true;
                            case R.id.pricelist:
                                startActivity(new Intent(Notification_Activity.this, Price_Activity.class));
                                return true;
                            case R.id.notifipop:
                                startActivity(new Intent(Notification_Activity.this, Notification_Activity.class));
                                return true;
                            case R.id.promo_scheem_pop:
                                startActivity(new Intent(Notification_Activity.this, Schemes_Offers_Page.class));
                                return true;
                            case R.id.whatpop:
                                startActivity(new Intent(Notification_Activity.this, WhatsNew_ApplicationActivity.class));
                                return true;
                            case R.id.disnetpop:
                                startActivity(new Intent(Notification_Activity.this, Distributer_Activity.class));
                                return true;
//                                if (Usertype.equals("Gates Employee")) {
//                                    Intent g = new Intent(Notification_Activity.this, LoginActivity.class).putExtra("from", "dist");
//                                    startActivity(g);
//                                } else {
//                                    box.showAlert(getString(R.string.not_authorized));
//                                }
//                                return true;
                            case R.id.contactpop:
                                startActivity(new Intent(Notification_Activity.this, Contact_Details_Activity.class));
                                return true;
//                            case R.id.editprofilepop:
//                                startActivity(new Intent(Product_Activity.this, EditProfileActivity.class));
//                                return true;

                        }

                        return false;
                    }
                });
                popupMenu.inflate(R.menu.popmenu);
                popupMenu.show();
            }
        });
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
    }


    private void checkinternet() {
        Network_Connection_Activity connection = new Network_Connection_Activity(Notification_Activity.this);

        if (connection.CheckInternet()) {
//            new BackroundRunning().execute();
            callApiServiceForNotification();

        } else {
            // Toast.makeText(Notification_Activity.this, "NO INTERNATE", Toast.LENGTH_SHORT).show();
            alertbox.showAlertWithBack("Unable to connect Internet. Please check your Internet connection !");
        }


    }

    private void callApiServiceForNotification() {
        final CustomProgressDialog progressDialog = new CustomProgressDialog(Notification_Activity.this);
        progressDialog.setCancelable(false);
        progressDialog.show();

        try {
            APIService service = RetrofitClient.getApiService();
            Call<NotificationModel> call = service.getNofication();

            call.enqueue(new Callback<NotificationModel>() {
                @Override
                public void onResponse(Call<NotificationModel> call, Response<NotificationModel> response) {
                    progressDialog.dismiss();
                    try {
                        if ("Success".equals(response.body().getResult())) {
                            adapter = new Notification_Adapter(Notification_Activity.this, response.body().getData());
                            n_listview.setAdapter(adapter);
                        } else {
                            alertbox.showAlertWithBack("No Record Found");
                        }
                    } catch (Exception e) {
                        progressDialog.dismiss();
                        e.printStackTrace();
                        alertbox.showAlertWithBack("Invalid Response. Please try again Later");
                    }
                }

                @Override
                public void onFailure(Call<NotificationModel> call, Throwable t) {
                    progressDialog.dismiss();
                    alertbox.showAlertWithBack("Failed to Reach the Server. Please try again Later");

                }
            });
        } catch (Exception e) {
            progressDialog.dismiss();
            e.printStackTrace();
            alertbox.showAlertWithBack("Exception Occurred. Please try again Later");
        }
    }


//    class BackroundRunning extends AsyncTask<String, Void, String> {
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//
//            progressDialog = new ProgressDialog(Notification_Activity.this);
//            // spinner (wheel) style dialog
//            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//            progressDialog.setCancelable(false);
//            progressDialog.setMessage("Loading...");
//            progressDialog.show();
//            //   s1=mobilenumber.getText().toString();
//            //  s2=password.getText().toString();
//
//        }
//
//        @Override
//        protected String doInBackground(String... strings) {
//
//            Server_Connection_Activity server=new Server_Connection_Activity();
//            try {
//                connection=server.getconnection();
//
//                stmt=connection.createStatement();
//
//                //and Usertype= '"+Usertype+"'
//
//                String selectquery= "SELECT distinct * from Notificationdetails where ActiveNotification='Active' and deleteflag='notdelete' and Usertype= '"+ Usertype +"' or ActiveNotification='Active' and deleteflag='notdelete' and Usertype='All Users' order by Notificationdetails_id desc ";
//                resultSet = stmt.executeQuery(selectquery);
//                while (resultSet.next())
//                {
//                    n_name.add(resultSet.getString("NotificationName"));
//                    n_descb.add(resultSet.getString("Description"));
//
//                    n_exp_date.add(resultSet.getString("NotificationExpiryDate"));
//                   // select_url.add(resultSet.getString("videoURL"));
//                }
//                if(n_name.isEmpty())
//                {
//                    return "empty";
//                }
//
//                resultSet.close();
//                connection.close();
//                stmt.close();
//
//                return "Sucess";
//
//            } catch (Exception e) {
//                e.printStackTrace();
//                return "error";
//            }
//        }
//        @Override
//        protected void onPostExecute(String s) {
//            super.onPostExecute(s);
//            progressDialog.dismiss();
//            if (s.equals("Sucess"))
//            {
//                Notification_Adapter product_details;
//                product_details = new Notification_Adapter(Notification_Activity.this,n_name,n_descb,n_exp_date);
//
//                n_listview.setAdapter(product_details);
//            }
//            else if(s.equals("empty"))
//            {
//                showdialog("Sorry! No notifications available");
//            }
//
//            else
//            {
//                showdialog("Unable to connect Internet. Please check your Internet connection !");
//            }
//        }
//    }

}











