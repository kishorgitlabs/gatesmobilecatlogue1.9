package com.brainmagic.gatescatalog.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.brainmagic.gatescatalog.gates.R;
import com.brainmagic.gatescatalog.activities.productsearch.Product_Activity;
import com.brainmagic.gatescatalog.activities.productsearch.whatsnew.WhatsNew_ApplicationActivity;
import com.brainmagic.gatescatalog.alertbox.Alertbox;

public class Contact_Details_Activity extends Activity {

    ImageView back,home,menu;
    SharedPreferences myshare;
    SharedPreferences.Editor editor;
    String Email, Mobno,Usertype;
    TextView cd_1,cd_2,cd_3,cd_4;
    Button map_button;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact__details_);

        back=(ImageView)findViewById(R.id.back);
        home=(ImageView)findViewById(R.id.home);
        menu=(ImageView)findViewById(R.id.menu);

        cd_1=(TextView)findViewById(R.id.cd_1);
        cd_2=(TextView)findViewById(R.id.cd_2);
        cd_3=(TextView)findViewById(R.id.cd_3);
        cd_4=(TextView)findViewById(R.id.cd_4);

        map_button=(Button)findViewById(R.id.map_button);

        cd_2.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
            String number = "+91.44.47115100";
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:" +number));
            startActivity(intent);
    }
});

       /* cd_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent gates_com=new Intent(Intent.ACTION_VIEW,Uri.parse("www.Gates.com."));
                startActivity(gates_com);
            }
        });*/
        myshare = getSharedPreferences("Registration",MODE_PRIVATE);
        editor = myshare.edit();

        Email=myshare.getString("Email","");
        Mobno=myshare.getString("MobNo","");
        Usertype=myshare.getString("Usertype","");


        map_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent searchAddress = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:0,0?q=" + "GATES UNITTA INDIA COMPANY PRIVATE LIMITED , F-19, SIPCOT Industrial Area,Pondur - A,Sriperumbudur,Kanchipuram District, Tamil Nadu,"));
                startActivity(searchAddress);
            }
        });


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Contact_Details_Activity.this, HomeScreenActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK)
                );
            }
        });


        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(Contact_Details_Activity.this, view);

                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });


                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub
                        switch (item.getItemId()) {
                            case R.id.homepop:
                                Intent home = new Intent(Contact_Details_Activity.this, HomeScreenActivity.class);
                                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(home);
                                return true;

                            case R.id.aboutpop:
                                startActivity(new Intent(Contact_Details_Activity.this, About_Gate_Activity.class));
                                return true;
                            case R.id.ptoductpop:
                                startActivity(new Intent(Contact_Details_Activity.this, Product_Activity.class));
                                return true;
                            case R.id.other_link_prod:
                                Intent a = new Intent(Contact_Details_Activity.this, OtherLinksActivity.class);
                                startActivity(a);
                                return true;
//                            case R.id.vediopop:
//                                if (!Usertype.equals("")) {
//                                    Intent f = new Intent(Contact_Details_Activity.this, Video_Category_Activity.class);
//                                    startActivity(f);
//                                }
//                                return true;
                            case R.id.notifipop:
                                startActivity(new Intent(Contact_Details_Activity.this, Notification_Activity.class));
                                return true;

                            case R.id.pricelist:
                                startActivity(new Intent(Contact_Details_Activity.this, Price_Activity.class));
                                return true;
                            case R.id.promo_scheem_pop:
                                startActivity(new Intent(Contact_Details_Activity.this, Schemes_Offers_Page.class));
                                return true;
                            case R.id.whatpop:
                                startActivity(new Intent(Contact_Details_Activity.this, WhatsNew_ApplicationActivity.class));
                                return true;
                            case R.id.disnetpop:
                                startActivity(new Intent(Contact_Details_Activity.this, Distributer_Activity.class));
                                return true;
//                                if (Usertype.equals("Gates Employee")) {
//                                    Intent g = new Intent(Contact_Details_Activity.this, LoginActivity.class).putExtra("from", "dist");
//                                    startActivity(g);
//                                } else {
//                                    box.showAlert(getString(R.string.not_authorized));
//                                }
//                                return true;
//                            case R.id.contactpop:
//                                startActivity(new Intent(Contact_Details_Activity.this, Contact_Details_Activity.class));
//                                return true;
//                            case R.id.editprofilepop:
//                                startActivity(new Intent(Product_Activity.this, EditProfileActivity.class));
//                                return true;

                        }

                        return false;
                    }
                });
                popupMenu.inflate(R.menu.popmenu);
                popupMenu.show();


            }
        });


    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
    }




    public void showdialog(String s) {

        Alertbox alert=new Alertbox(Contact_Details_Activity.this);
//        alert.Nointernet(s);
    }

    public void NointernetNOBACK(String s) {

        Alertbox alert=new Alertbox(Contact_Details_Activity.this);
        alert.NointernetNOBACK(s);
    }

}
