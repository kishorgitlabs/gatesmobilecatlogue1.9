package com.brainmagic.gatescatalog.adapter.threeepandablelist;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.Toast;

import com.brainmagic.gatescatalog.alertbox.Alertbox;
import com.brainmagic.gatescatalog.gates.R;
import com.brainmagic.gatescatalog.api.models.moredetails.ModelDetailsResult;
import com.brainmagic.gatescatalog.font.FontDesign;

import java.util.List;

@SuppressWarnings("unchecked")
public class ParentMakeExAdapter extends BaseExpandableListAdapter {
    private final Context mContext;
    private List<ModelDetailsResult> parentMakeListModel;

    public ParentMakeExAdapter(Context mContext, List<ModelDetailsResult> parentMakeListModel) {
        this.mContext = mContext;
        this.parentMakeListModel = parentMakeListModel;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View view, ViewGroup viewGroup) {
        View cView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_parent_make_expandable_list_view, viewGroup, false);

        try {
            String make = parentMakeListModel.get(groupPosition).getMake();

            FontDesign model = cView.findViewById(R.id.heading_expandable_list);
            ImageView exArrow = cView.findViewById(R.id.expandable_arrow);
            exArrow.setVisibility(View.VISIBLE);
            if (isExpanded)
                exArrow.animate().rotation(180).setInterpolator(new LinearInterpolator()).setDuration(500);
            else
                exArrow.animate().rotation(0).setInterpolator(new LinearInterpolator()).setDuration(500);
            model.setText(make);
        } catch (Exception e) {
            e.printStackTrace();
           Toast.makeText(mContext, "make view:" +e.toString(), Toast.LENGTH_LONG).show();
        }
        return cView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean b, View view, ViewGroup viewGroup) {
        try {
//            Toast.makeText(mContext, "Error 1", Toast.LENGTH_SHORT).show();
            final CustomExpListView secondLevelExpListView = new CustomExpListView(this.mContext);
//        String parentNode = (String) getGroup(groupPosition);
//            Toast.makeText(mContext, "Error 2", Toast.LENGTH_SHORT).show();
            secondLevelExpListView.setAdapter(new ChildModelExAdapter(this.mContext, parentMakeListModel.get(groupPosition).getModelList()));

//            Toast.makeText(mContext, "Error 3", Toast.LENGTH_SHORT).show();
            secondLevelExpListView.setGroupIndicator(null);

//            Toast.makeText(mContext, "Error 4", Toast.LENGTH_SHORT).show();
//        secondLevelExpListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
//            int previousGroup = -1;
//            @Override
//            public void onGroupExpand(int groupPosition) {
//                if (groupPosition != previousGroup)
//                    secondLevelExpListView.collapseGroup(previousGroup);
//                previousGroup = groupPosition;
//            }
//        });

            return secondLevelExpListView;
        }catch (Exception e)
        {
            Toast.makeText(mContext, "Error 5"+e.getMessage(), Toast.LENGTH_SHORT).show();
            return null;
        }
    }


    @Override
    public int getGroupCount() {
        return parentMakeListModel.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return parentMakeListModel.get(groupPosition).getModelList().size();
//        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return parentMakeListModel.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return true;
    }
}
